
const { query } = require("express");
const fs = require("fs");
const express = require("express");
const app = express();

app.use(express.urlencoded({ extended: true }));

app.use(express.static("public"));

app.get("/", (req, res) => {
  res.sendFile(__dirname + "/public/html/index.html");
});

app.get("/alunos", (req, res) => {
  res.sendFile(__dirname + "/public/html/aluno.html");
});

app.get("/search", (req, res) => {
  const select = req.query.select;
  let newJsonAlunos = [];
  fs.readFile(__dirname + "/alunos.json", "utf8", (err, data) => {
    const alunos = JSON.parse(data);
    for (i in alunos) {
      if (alunos[i].nome.toLowerCase().includes(select.toLowerCase())) {
        newJsonAlunos.push(alunos[i]);
      }
    }
    return res.json(newJsonAlunos);
  });
});

app.get("/duvida", (req, res) => {
  res.sendFile(__dirname + "/public/html/duvida.html");
});

app.get("/eu", (req, res) => {
  res.sendFile(__dirname + "/public/html/eu.html");
});

app.get("/confirmacao", (req, res) => {
  const email = req.query.email;
  const mensagem = req.query.mensagem;
  const styleDiv = `width: 50%; height:50%; display: flex;
    margin: 50px auto; 
    align-items: center; justify-content: center;
    flex-direction:column; font-family: sans-serif;`;

  const styleAnchor = `color: #000; font-family: Georgia, 'Times New Roman', Times, serif;
        text-decoration: none; font-size: 20px;
        align-items: center;
        font-weight: bold;`;

  res.send(
    `
        <div style="${styleDiv}">
            <p style = "font-size: 20px; text-align: center; padding-top:15%">
            Olá, a dúvida "${mensagem.italics()}" foi recebida.<br>
             Retornaremos no e-mail ✖ ${email.italics()} <br>
            <a href="/" style="${styleAnchor}">
                ↣Clique aqui para voltar a tela inicial↢ 
            </a>
        </div>
        `
  );
});

app.listen(3000, () => {
  console.log("Server aberto em: http://localhost:3000/");
});